# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 07:40:35 2020

@author: Ranak Roy Chowdhury
"""
# 3rd change by ranak
from numpy import loadtxt
import joblib
import numpy as np
import utils
import random
import sys


class KernelShap:
    def __init__(self, trained_model, X_reference, problem_type, cat_var, nsamples = None, summarise_result = None, reference_samples = None, regularization_rate = None):
        
        # All inputs to KernelShap
        self.trained_model = trained_model
        self.X_reference = X_reference
        
        if problem_type == 'mcc' or problem_type == 'bc' or problem_type == 'r':
            self.problem_type = problem_type
        else:
            print(str(problem_type) + ' is an invalid problem type specification.')
            sys.exit()
        
        if type(summarise_result) is bool:
            self.summarise_result = summarise_result
        else:
            self.summarise_result = True
        
        # check if the conditions may be violated, then default input, otherwise user input
        if reference_samples is None or type(reference_samples) is not int or reference_samples <= 0 or reference_samples > self.X_reference.shape[0]:
            self.reference_samples = self.X_reference.shape[0]
        else:
            self.reference_samples = int(reference_samples)
        l = random.sample(range(0, self.X_reference.shape[0]), self.reference_samples)
        self.X_reference = self.X_reference[l, : ]
        
        '''
        # set nclasses as per the type of problem
        if self.problem_type == 'mcc':
            if type(nclasses) is not int or nclasses < 3:
                print(str(nclasses) + ' is an invalid number of classes for multiclass classification.')
                sys.exit()
            else:
                self.nclasses = int(nclasses)
        elif self.problem_type == 'bc':
            self.nclasses = 2
        elif self.problem_type == 'r':
            self.nclasses = 1
        '''
        
        if cat_var is None:
            self.cat_var = {}
        else:
            self.cat_var = cat_var
        
        # check if all the keys and values in dictionary are integers
        for key in cat_var.keys():
            if type(key) is not int or type(cat_var[key]) is not int or key < 0 or cat_var[key] < 1:
                print('Key Value Pair - ' + str(key) + ' : ' + str(cat_var[key]) + ' is invalid')
                sys.exit()
                
        self.features = self.X_reference.shape[1] - sum(self.cat_var.values()) + len(self.cat_var.keys())
        
        if nsamples is None or type(nsamples) is not int or nsamples <= 0:
            self.nsamples = 2 * self.features + 2 ** 4
        else:
            self.nsamples = int(nsamples)
        
        if regularization_rate is None or type(regularization_rate) is not float or regularization_rate <= 0:
            self.regularization_rate = 0.01
        else:
            self.regularization_rate = regularization_rate
  
      
        # All intermediate data structures
        # returns a matrix of 1's and 0's indicating which features will be absent or present in each perturbed instance
        self.coalition = utils.coalify(self.features, self.nsamples)
        
        # assign weight to each coalition, return the weight diagonal matrix
        self.weight = utils.proximity(self.coalition)
        
        # All outputs from KernelShap
        self.output = {}
        
        # Expected Value of the predictions from the background or reference data
        self.output['expected_value'] = utils.predict(self.problem_type, self.trained_model, self.X_reference)
        self.nclasses = self.output['expected_value'].shape[1]
        self.output['expected_value'] = np.mean(self.output['expected_value'], axis = 0)
        print('Expected Values: ' + str(self.output['expected_value']))
        
        # Raw predictions from  the test data
        self.output['raw_prediction'] = np.zeros((1, self.nclasses))
        
        # Unsorted shap values in a numpy array
        if self.summarise_result is True:
            self.output['shap_values'] = np.zeros((self.nclasses + 1, self.features))
        else:
            self.output['shap_values'] = np.zeros((self.nclasses + 1, self.X_reference.shape[1]))
            
        # list of dictionaries, each dictionary contains shap values in descending order
        self.output['sorted_shap_values'] = None
        
        
        
    def explain(self, data): 
        # raw prediction on the data
        self.output['raw_prediction'] += utils.predict(self.problem_type, self.trained_model, data)
        
        # map the coalitions to data values
        perturbed_data = utils.mapping(data, self.coalition, self.X_reference, self.features, self.cat_var) 
            
        # get the predictions for the perturbed data
        pred_values = utils.predict(self.problem_type, self.trained_model, perturbed_data)
            
        # convert the predicted probabilities to raw logit values for classification, otherwise get pred_values
        # pred_values = utils.convert_to_logit(self.problem_type, pred_values)
        
        # compute the shap values for each feature of each class through a linear model
        self.output['shap_values'] += utils.computeShapValues(perturbed_data, self.weight, pred_values, self.features, self.regularization_rate, self.summarise_result, self.cat_var)


        
        
trained_model = joblib.load('trained_model') 
X_reference = loadtxt('X_reference.csv', delimiter=',')  
        
        
#-------------------------SELECT, FROM------------------------#        
X = loadtxt('X.csv', delimiter=',')


#----------------------------WHERE----------------------------#
idx = 4 
X = X[idx : idx + 6, : ]


#------------ WITH: User defined mandatory inputs------------#
# problem_type can be 'mcc', 'bc' or 'r'
# this is needed to know whether the model is XGBClassifier or XGBRegressor
# XGBClassifier has predict_proba which I'll need for classification problems and I may have to call logit function on the values returned by predict_proba
# XGBRegressor just has predict function and no need to call logit on the values returned by predict function but needs reshaping
problem_type = 'mcc'

# If there are categorical variables in the data then it must be specified as dic.
# Since there is no preprocessor, one-hot encoded data is taken as input.
# Need to know which features are categorical variables and their dimensions.
cat_var = {1 : 2, 5 : 3} # dictionary


#------------- WITH: User defined optional inputs------------#
#nsamples = 50  # also include a bias term for regression, also consider subtracting raw prediction from expected prediction to get the regression label, also consider changing data from n *d to n * (d+1) with a bias ter, # try out scikit Linear Regression
#summarise_result = False # True or False  
#reference_samples = 50 
#regularization_rate = 10 # any floating point value


#------------------------USING SHAP-----------------------#
# algorithm initialization doesn't depend on the data selected from SELECT clause
explainer = KernelShap(trained_model, X_reference, problem_type = problem_type, cat_var = cat_var)

# for every row of selected data
for i in range(X.shape[0]):
    explainer.explain(X[i : i + 1, : ])
    

#-----------------------STORE RESULT----------------------#
explainer.output['raw_prediction'] /= X.shape[0]
print('Raw Prediction: ' + str(explainer.output['raw_prediction']))

explainer.output['shap_values'] /= X.shape[0]
#print(np.sum(self.output['shap_values'], axis = 1))
#print(self.output['shap_values']) 
#print(self.output['shap_values'].shape)
        
# sort the shap values in descending order
explainer.output['sorted_shap_values'] = utils.sorting(np.abs(explainer.output['shap_values']))
print('Output SHAP Table: ' + str(explainer.output['sorted_shap_values']))
