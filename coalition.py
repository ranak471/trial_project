# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 22:13:13 2020

@author: Ranak Roy Chowdhury
"""
import numpy as np
from itertools import combinations


def generateCombinations(N, C):
    comb = combinations(N, C) # C out of N features will be 1 
    comb = list(comb)
    inv_comb = [set(N) - set(x) for x in comb] # N - C out of N features will be 1
    inv_comb = [tuple(x) for x in inv_comb]
    comb = comb + inv_comb # combine the two
    return comb
    
    
def generateCoalitions(comb):
    coalition = np.zeros((len(comb), features))
    for i in range(len(comb)):
        coalition[i, comb[i]] = 1
    return coalition
    
            
# returns a matrix of 1's and 0's indicating which features will be absent or present in each perturbed instance
def coalify():
    samples = 0
    N = list(range(0, features))
    C = 1
    coalition = np.zeros((1, features))
        
    while samples < nsamples:
        comb = generateCombinations(N, C)
        comb = comb[0 : nsamples - samples] # select as many samples as permitted by self.nsamples
        coalition_C = generateCoalitions(comb)
        coalition = np.concatenate((coalition, coalition_C), axis = 0)
        samples += len(comb) # increase the number of samples taken
        C += 1 
            
    return coalition[1 : , : ]


features = 5
nsamples = 35
coalition = coalify()
print(coalition)