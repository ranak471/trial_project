# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 00:39:30 2020

@author: Ranak Roy Chowdhury
"""
import math
import numpy as np
from itertools import combinations


def logit(x):
    return math.log(x/(1-x)) # should be natural logarithm ln, given by math.log in python


# given the problem type, trained model and data, predict the probability values for classification and raw logit value for regression
def predict(problem_type, trained_model, data):
    if problem_type == 'mcc' or problem_type == 'bc':
        pred_values = trained_model.predict_proba(data)
        #pred_values = np.vectorize(logit)(pred_values)
    elif problem_type == 'r':
        pred_values = trained_model.predict(data)
        pred_values = pred_values.reshape(pred_values.shape[0], 1)
    return pred_values

    
def generateCombinations(N, C):
    comb = combinations(N, C) # C out of N features will be 1 
    comb = list(comb)
    inv_comb = [set(N) - set(x) for x in comb] # N - C out of N features will be 1
    inv_comb = [tuple(x) for x in inv_comb]
    comb = comb + inv_comb # combine the two
    return comb
    
    
def generateCoalitions(features, comb):
    coalition_C = np.zeros((len(comb), features))
    for i in range(len(comb)):
        coalition_C[i, comb[i]] = 1
    return coalition_C
    
            
# returns a matrix of 1's and 0's indicating which features will be absent or present in each perturbed instance
def coalify(features, nsamples):
    samples = 0
    N = list(range(0, features))
    C = 1
    coalition = np.zeros((1, features))
        
    while samples < nsamples:
        comb = generateCombinations(N, C)
        comb = comb[0 : nsamples - samples] # select as many samples as permitted by self.nsamples
        coalition_C = generateCoalitions(features, comb)
        coalition = np.concatenate((coalition, coalition_C), axis = 0)
        samples += len(comb) # increase the number of samples taken
        C += 1 
        # C can grow from 1 to features - 1, because otherwise if C = features, then C choose features will mean all 1's and all 0's.
        if C == features:
            C = 1
    return coalition[1 : , : ]


# (#features - 1) / (#featuresC#ones * #ones * (#features - #ones))
def computeWeight(coalition_i, N):
    C = int(sum(coalition_i))
    comb = len(list(combinations(N, C))) # C out of N features will be 1 
    M = len(N)
    w = (M - 1) / (comb * C * (M - C))
    return w
    
    
# assign weight to each coalition, return the weight diagonal matrix
def proximity(coalition):
    n, features = coalition.shape
    N = list(range(0, features))
    weight = []
    
    for i in range(n):
        w = computeWeight(coalition[i], N)
        weight.append(w)
        
    return np.diag(weight)
    
        
# map the coalitions to data values
def mapping(data, coalition, X_reference, features, cat_var):
    l = []
    l.append(data[0])
    
    for i in range(coalition.shape[0]):
        perturbed_data = []
        feature = 0
        
        # find each of the feature value for the perturbed data
        for j in range(features):
            # coalition[i, j] = 1 means feature is present in data and feature is a categorical variable
            if coalition[i, j] == 1 and j in cat_var.keys():
                
                # copy the one-hot encoded form of the categorical feature from the data
                for k in range(cat_var[j]):
                    perturbed_data.append(data[0, feature])
                    feature += 1
                        
            # coalition[i, j] = 1 means feature is present in data and feature is not a categorical variable
            elif coalition[i, j] == 1 and j not in cat_var.keys(): 
                perturbed_data.append(data[0, feature]) # copy the feature from the data
                feature += 1
                    
            # coalition[i, j] = 0 means feature not present in data and feature is a categorical variable
            elif coalition[i, j] == 0 and j in cat_var.keys(): 
                index = np.random.choice(X_reference.shape[0], 1, replace=False) # choose which row of X_reference will be sampled
                
                # sample the one-hot encoded form of the categorical feature from the chosen row of X_reference
                for k in range(cat_var[j]):
                    perturbed_data.append(X_reference[index[0], feature])
                    feature += 1
                        
            # coalition[i, j] = 0 means feature not present in data and feature is not a categorical variable
            elif coalition[i, j] == 0 and j not in cat_var.keys(): 
                index = np.random.choice(X_reference.shape[0], 1, replace=False) # choose which row of X_reference will be sampled
                perturbed_data.append(X_reference[index[0], feature]) # sample the j-th feature from the chosen row of X_reference
                feature += 1
                    
        l.append(perturbed_data)
        
    l = np.array(np.stack(l, axis = 0))
    return l[1 : , : ]


def regression(data, weight, label, features, regularization_rate):
    n, d = data.shape
    n, nclasses = label.shape
    raw_shap_values = []
    
    # compute shap values for all the features of all the classes
    for i in range(nclasses):
        # compute the shap value for class i
        shap_class = np.matmul(np.linalg.inv(np.matmul(np.matmul(data.T, weight), data) + regularization_rate * np.identity(d)), np.matmul(np.matmul(data.T, weight), label[ : , i : i + 1]))
        raw_shap_values.append(shap_class[ : , 0])
    raw_shap_values = np.array(np.stack(raw_shap_values, axis = 0))
    
    # compute the aggregate shap value for each feature
    s = np.sum(raw_shap_values, axis = 0)
    s = s.reshape(1, d)
    return np.concatenate((raw_shap_values, s), axis = 0)
    
    
def summarise(raw_shap_values, features, summarise_result, cat_var):
    # summarise shap values based on categorical features
    shap_values = []
    
    if summarise_result:
        for i in range(features):
            if i in cat_var.keys():
                shap_values.append(np.sum(raw_shap_values[ : , i : i + cat_var[i]], axis = 1))
            else:
                shap_values.append(raw_shap_values[ : , i])
        shap_values = np.array(np.stack(shap_values, axis = 0))
        shap_values = shap_values.T
    else:
        shap_values = np.copy(raw_shap_values)
        
    return shap_values
    
    
def computeShapValues(data, weight, label, features, regularization_rate, summarise_result, cat_var):
    raw_shap_values = regression(data, weight, label, features, regularization_rate)
    shap_values = summarise(raw_shap_values, features, summarise_result, cat_var)
    return shap_values


def buildDic(shap_values):
    row, column = shap_values.shape
    shap_dic = []
    
    for i in range(row):
        d = {}
        for j in range(column):
            d[j] = shap_values[i, j]
        shap_dic.append(d)
        
    return shap_dic
    

def sorting(shap_values):
    row, column = shap_values.shape
    sorted_shap_values = buildDic(shap_values) # list of dictionaries, each dictionary contains shap values in descending order
    
    # sort each dictionary by its value
    for i in range(row):
        sorted_shap_values[i] = {k: v for k, v in sorted(sorted_shap_values[i].items(), key=lambda item: item[1], reverse = True)}
    
    return sorted_shap_values