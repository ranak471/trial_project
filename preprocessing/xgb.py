# -*- coding: utf-8 -*-
"""
Created on Sat Aug  1 05:31:24 2020

@author: Ranak Roy Chowdhury
"""
# 1st change by ranak

import numpy as np
import xgboost as xgb
from numpy import savetxt
import joblib


def one_hot_encoding(obs, dim):
    val = np.random.randint(0, dim, obs)
    mat = np.zeros((obs, dim))
    rows = np.arange(obs)
    mat[rows, val] = 1
    return mat

    
def prepare_data(obs):
    data = np.zeros((obs, 1))
    for feat in range(features):
        if feat in cat_var.keys():
            feat_data = one_hot_encoding(obs, cat_var[feat])
        else:
            feat_data = np.random.rand(obs, 1)
        data = np.concatenate((data, feat_data), axis = 1)
    return data[ : , 1 : ]


def set_label(obs):
    if problem_type == 'mcc':
        train_y = np.random.randint(0, classes, obs) #four classes:0,1,2,3
    elif problem_type == 'bc':
        train_y = np.random.randint(0, 2, obs) #two classes: 0,1
    elif problem_type == 'r':
        train_y = np.random.rand(obs)
    return train_y


def declare_model():
    if problem_type == 'mcc' or problem_type == 'bc':
        xgb_model = xgb.XGBClassifier(max_depth= 3, n_estimators=100)
    elif problem_type == 'r':
        xgb_model = xgb.XGBRegressor(max_depth= 3, n_estimators=100)
    return xgb_model


def evaluate(data, target_label):
    print('Data: ' + str(data))
    print('Target Value: ' + str(target_label))
    print('Predicted Value: ' + str(xgb_model.predict(data)))
    if problem_type == 'mcc' or problem_type == 'bc':
        print('Class Labels: ' + str(xgb_model.classes_))
        print('Predicted Probabilities: ' + str(xgb_model.predict_proba(data)))


def generate_data():
    # generate dummy data (10 numeric features, 4 classes of target)
    np.random.seed(312)
    
    train_X = prepare_data(train_inst)
    train_y = set_label(train_inst)
    savetxt('X_reference.csv', train_X, delimiter=',')
    
    test_X = prepare_data(test_inst)
    test_y = set_label(test_inst)
    savetxt('X.csv', test_X, delimiter=',')

    return train_X, train_y, test_X, test_y


# Set problem type
problem_type = 'mcc' # mcc = multicalss classification, bc = binary classification, r = regression
train_inst = 10000
test_inst = 100

classes = 3
features = 10
cat_var = {1 : 2, 5 : 3}


train_X, train_y, test_X, test_y = generate_data()
xgb_model = declare_model()
xgb_model.fit(train_X, train_y)
joblib.dump(xgb_model, 'trained_model')
evaluate(train_X[0 : 1], train_y[0])