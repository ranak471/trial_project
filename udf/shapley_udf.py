# -*- coding: utf-8 -*-
"""
Created on Sat Aug 29 13:41:15 2020

@author: Ranak Roy Chowdhury
"""
# 2nd change by ranak

def shapley(x_feat_0, x_feat_1, x_feat_2, x_feat_3, z_feat_0, z_feat_1, z_feat_2, z_feat_3, j, c):
    import numpy as np
    x = np.array([x_feat_0, x_feat_1, x_feat_2, x_feat_3])
    z = np.array([z_feat_0, z_feat_1, z_feat_2, z_feat_3])
    print(x)
    print(z)
    
    perm = np.random.permutation(len(x)) # or len(z)
    print(perm)
    x_zero = x[perm]
    z_zero = z[perm]
    print(x_zero)
    print(z_zero)
    
    idx = np.where(x_zero == x[j])[0][0]
    x_plus_j = np.hstack((x_zero[ : idx + 1], z_zero[idx + 1 : ]))
    x_minus_j = np.hstack((x_zero[ : idx], z_zero[idx : ]))
    print(x_plus_j)
    print(x_minus_j)
    
    unperm = np.argsort(perm)
    unperm_x_plus_j = x_plus_j[unperm]
    unperm_x_minus_j = x_minus_j[unperm]
    print(unperm_x_plus_j)
    print(unperm_x_minus_j)
    
    return sum(unperm_x_plus_j) - sum(unperm_x_minus_j)


a = shapley(23, 45, 67, 34, 91, 657, 761, 293, 2, 0)